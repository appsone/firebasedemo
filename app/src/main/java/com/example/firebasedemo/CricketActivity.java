package com.example.firebasedemo;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class CricketActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cricket);

        WebView webView = findViewById(R.id.webViewCricket);
        Button back = findViewById(R.id.back);


        //webView.setWebViewClient(new MyWebViewClient(this));
        //webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://www.cricbuzz.com/");

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView viewx, String urlx) {
                viewx.loadUrl(urlx);
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CricketActivity.super.onBackPressed();
            }
        });
    }

    /*public class MyWebViewClient extends WebViewClient {

        public MyWebViewClient(CricketActivity cricketActivity) {
            super();
            //start anything you need to
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //Do something to the urls, views, etc.

        }
    }*/
}