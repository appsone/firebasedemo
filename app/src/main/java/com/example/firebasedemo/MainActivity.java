package com.example.firebasedemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {


    private Button cricket, football, motogp;
    private TextView fatal, nonfatal;
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 101);
        }else
        {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Please add permissions", Snackbar.LENGTH_SHORT);
        }

        cricket = findViewById(R.id.imgButton1);
        football = findViewById(R.id.imgButton2);
        motogp = findViewById(R.id.imgButton3);

        fatal = findViewById(R.id.fatalCrash);
        nonfatal = findViewById(R.id.nonfatalCrash);

        //imageView = findViewById(R.id.imageView);

        cricket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //loadImageFromWeb(getString(R.string.cricket));
                //Picasso.get().load(getString(R.string.cricket)).into(imageView);
                Intent intent = new Intent(MainActivity.this, CricketActivity.class);
                //intent.putExtra("url","https://www.motorsport.com/motogp/standings/");
                startActivity(intent);
            }
        });

        football.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //loadImageFromWeb(getString(R.string.football));

                //new DownloadImageTask(imageView).execute(getString(R.string.football));

                Intent intent = new Intent(MainActivity.this, FootballActivity.class);
                //intent.putExtra("url","https://www.motorsport.com/motogp/standings/");
                startActivity(intent);
            }
        });

        motogp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Picasso.with(MainActivity.this).load(getString(R.string.motogp)).into(imageView);
                //loadImageFromWeb(getString(R.string.motogp));
                //new DownloadImageTask(imageView).execute(getString(R.string.motogp));

                Intent intent = new Intent(MainActivity.this, MotogpActivity.class);
                //intent.putExtra("url","https://www.motorsport.com/motogp/standings/");
                startActivity(intent);
            }
        });

        fatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                throw new NullPointerException();
            }
        });

        nonfatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    throw new IllegalAccessException();
                } catch (Exception e) {
                    //FirebaseCrashlytics.getInstance().recordException(e);    //Recording Exceptions in Firebase

                }
            }
        });


    }

    private void loadImageFromWeb(String IMAGE_URL) {
        Glide.with(this).
                load(IMAGE_URL)
                .placeholder(new ColorDrawable(ContextCompat.getColor(this, R.color.colorAccent)))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(
                            Exception e, String model, Target<GlideDrawable> target,
                            boolean isFirstResource) {
                       // mNumStartupTasks.countDown(); // Signal end of image load task.
                            return false;
                    }

                    @Override
                    public boolean onResourceReady(
                            GlideDrawable resource, String model, Target<GlideDrawable> target,
                            boolean isFromMemoryCache, boolean isFirstResource) {
                       // mNumStartupTasks.countDown(); // Signal end of image load task.
                        return false;
                    }
                }).into(imageView);
    }



    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bmp = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bmp = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return bmp;
        }
        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public static String getDeviceId(Context context) {

        String deviceId;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            deviceId = Settings.Secure.getString(
                    context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephony.getDeviceId() != null) {
                deviceId = mTelephony.getDeviceId();
            } else {
                deviceId = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            }
        }

        return deviceId;
    }

}